function getBotResponse(input) {

// Odgovori
// Pozdravi
    if (input.toLowerCase() == "dobar dan"){
        return "Dobar dan";
    } else if (input.toLowerCase() == "dovidjenja" || input.toLowerCase() == "doviđenja") {
        return "Doviđenja";
    } else if (input.toLowerCase() == "ćao" || input.toLowerCase() == "cao") {
        return "Zdravo"
    } else if (input.toLowerCase() == "pozdrav") {
        return "Pozdrav"
    } else if (input.toLowerCase() == "zdravo") {
        return "Zdravo"
    } else if (input.toLowerCase() == "dobro vece" || input.toLowerCase() == "dobro veče"){
        return "Dobro veče"
    }
// Odgovori na pitanja
    else if (input.toLowerCase().includes("zakazem") || input.toLowerCase().includes("zakažem")) {
        return "Da li ste pokušali dugme zakaži pregled na početnoj stranici ovog sajta?"
    }
    else {
        return "Robot ne moze dati odgovor na postavljeno pitanje. Možda odgovor možete pronaći klikom na dugme 'Često postavljena pitanja' na početnoj stranici ovog sajta.";
    }
}